anepsion
========

Physical server hosted at Hetzner, model EX41S.

Specifications
--------------

- 64GB DDR4 RAM
- Quad-core Intel i7-6700
- 2x 4TB HDD
- Ubuntu 18.04 (bionic)

Containers
----------

LXC containers in this server:

- :doc:`halono`, hosts lxr
- :doc:`charlotte`, hosts Jenkins master (build.kde.org etc)
- :doc:`capona`

Storage
-------

Both disks have identical layout:

========= ===== ==========================
Partition Size  Use
========= ===== ==========================
sd[ab]1   2GB   raid1, swap
sd[ab]2   512MB raid1, /boot
sd[ab]3   50GB  raid1, /
sd[ab]4   -     MBR extended partition
sd[ab]5   1.8TB raid1, LVM physical volume
========= ===== ==========================

The LVM volume group is then divided as follows:

============= ====== ===================
Volume        Size   Use
============= ====== ===================
vg0-capona    200GB  capona container
vg0-halono    150GB  halono container
vg0-charlotte 1.22TB charlotte container
============= ====== ===================

There is still ~180GB of free space in the VG.

Backups
-------

There is nothing interesting in anepsion itself,
so it doesn't do any backups.
All backups are done inside the containers.

Name etymology
--------------

   `Anepsion <https://en.wikipedia.org/wiki/Anepsion>`__ is a genus of
   orb-weaver spiders first described by Embrik Strand in 1929.
