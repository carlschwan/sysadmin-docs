charlotte
=========

LXC container running in :doc:`anepsion`.

Services
--------

-  Jenkins masters

   -  https://build.kde.org
   -  https://binary-factory.kde.org
   -  https://build.neon.kde.org

-  Docker swarm of build nodes supporting https://build.kde.org and
   https://binary-factory.kde.org
-  OpenVPN server for network of build nodes
-  Simple file hosting for:

   -  https://build-artifacts.kde.org
   -  https://metadata.neon.kde.org

-  SFTP proxy to download.kde.org for allowing
   https://build.neon.kde.org access to pre-release tarballs

Name etymology
--------------

   `Charlotte's Web <https://en.wikipedia.org/wiki/Charlotte%27s_Web>`__
   is a children's novel by American author E. B. White and illustrated
   by Garth Williams; it was published on October 15, 1952, by Harper &
   Brothers. The novel tells the story of a livestock pig named Wilbur
   and his friendship with a barn spider named Charlotte.
