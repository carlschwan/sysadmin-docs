nephilia
========

Physical server hosted at Hetzner, model `EX41-SSD <http://web.archive.org/web/20180905021221/https://www.hetzner.com/dedicated-rootserver/ex41-ssd>`__.

Specifications
--------------

- 32GB DDR4 RAM
- Quad-core Intel i7-6700
- 2x 500 GB SSD
- Ubuntu 18.04 (bionic)

Containers
----------

LXC containers in this server: 

-  :doc:`edulis`
-  :doc:`komaci`
-  :doc:`nicoda`

Storage
-------

Both disks have identical layout:

========= ===== ============
Partition Size  Use
========= ===== ============
sd[ab]1   512MB raid1, /boot
sd[ab]2   465GB raid1, /
========= ===== ============

Backups
-------

There is nothing interesting in nephilia itself,
the important data is in the containers,
so nephilia has no backups.
Backups are done by each container.

Name etymology
--------------

Likely a typo for Nephila:
>\ `Nephila <https://en.wikipedia.org/wiki/Nephila>`__ is a genus of
araneomorph spiders noted for the impressive webs they weave. Nephila
consists of numerous species found in warmer regions around the world.
They are commonly called golden silk orb-weavers, golden orb-weavers,
giant wood spiders, or banana spiders.
