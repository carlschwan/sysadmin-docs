code
====

LXC container running in :doc:`recluse`.

Services
--------

- Phabricator
- [STRIKEOUT:Gitolite-based git.kde.org] (decommissioned in favor of GitLab)
- Gitolite-based git hosting for KDE Neon

Backups
-------

Backups are saved into Recluse's hetzner backup space.
Some things are still stored here and still being backed up,
but the canonical version has been moved to another server.
It's pending cleanup.
