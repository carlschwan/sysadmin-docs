.. sysadmin-docs documentation master file, created by
   sphinx-quickstart on Tue Jun  2 08:35:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sysadmin team's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Services
   :name: sec-services

   services/build-node
   services/gitlab
   services/identity
   services/mirrorbrain
   services/monitoring
   services/neon

.. toctree::
   :maxdepth: 1
   :caption: Providers
   :name: sec-providers

   providers/hetzner
   providers/bytemark
   providers/digitalocean

.. toctree::
   :maxdepth: 1
   :caption: Servers
   :name: sec-servers

   servers/ctenzi
   servers/arkyid
   servers/recluse
   servers/charlotte
   servers/code
   servers/nephilia
   servers/micrea
   servers/eucten
   servers/anepsion
   servers/overwatch
   servers/letterbox
   servers/orbi

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
