Mirror system
=============

Powered by `MirrorBrain <https://www.mirrorbrain.org/>`_.

.. todo::
   Document system setup. Document how to publish a new file.


Mirror requirements
-------------------

Requirements for becoming a mirror are listed at:
https://kde.org/mirrors/ftp_howto.php

Mirrors are required to provide (read-only) rsync access
for MirrorBrain to scan what files the mirror has,
but if a mirror admin doesn't want to provide public rsync to all users,
they may also choose to restrict access to only our server IP.


Adding a new mirror
-------------------

Login to milonia, under user account 'mirrorbrain'.

Ensure we have access to the mirror over rsync.
Running eg. ``rsync rsync://mirror.example.com/kde/``
should show a file list.

Run ``mb new`` to add the mirror:
``mb -b <instance> new <identifier> -c <country code> -H <http URL> -R <rsync URL>``

  * ``<instance>`` should be either ``download`` or ``files``
    depending on which dataset the mirror is for.
  * ``<identifier>`` is usually the mirror's hostname.
  * ``<country code>`` is the two-letter code of the country
    where the mirror is located.
  * There are also options ``-a "<admin name>" -e <admin email>``
    to set contact information.

More info is available with ``mb new --help``.

Example::

  mb -b download new mirror.example.com -c US -H http://mirror.example.com/kde/ -R rsync://mirror.example.com/kde/ -a "Joe Doe" -e "mirror-admin@example.com"

.. This dash works around an issue in the Aether theme for Sphinx

--

All information can be changed later with ``mb edit``.

Once the mirror is added to MirrorBrain's database,
it's still disabled.
We need to scan the mirror contents and enable it::

  mb -b <instance> scan --enable <identifier>

  # Example:
  mb -b download scan --enable mirror.example.com

This will use rsync to get a list of what files
are present in the mirror,
and then enable the mirror.

Loading the .mirrorlist URL of any individual file in the download server
should now list the new mirror.

The full mirror list (eg. https://download.kde.org/extra/download-mirrors.html)
may not show the new mirror yet,
because these pages are re-generated only once a day.
If you want to update them immediately,
run the ``mb mirrorlist`` command
configured in mirrorbrain's user crontab.
(It has lots of arguments,
I didn't want to put it in the documentation
and risk divergence,
so take it from the cronjob).
